/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 18:52:36 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/09 11:20:18 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "libft.h"
# include "limits.h"

typedef enum		e_type
{
	LEM,
	START,
	END,
	ROOM,
	TUBE,
	DUP,
	COMM,
	FAIL
}					t_type;

typedef struct		s_lst
{
	t_type			type;
	int				dst;
	char			*info;
	char			*side;
	struct s_lst	*path;
	struct s_lst	*pipe;
	struct s_lst	*road;
	struct s_lst	*next;
	struct s_lst	*rewind;
}					t_lst;

typedef struct		s_flags
{
	int				lem;
	int				l;
	int				r;
	int				s;
	int				e;
	struct s_lst	*lst;
}					t_flags;

t_lst				*ft_sort_lst(t_lst *lst);
t_lst				*ft_build_hex(t_lst *lst, t_lst **ret, t_lst ***tab);
t_lst				*ft_filter(t_lst *lst, int *lem);
t_lst				*ft_hndlhex(t_flags *flags, t_lst *tmp);
t_lst				*ft_dispatch(t_lst **lst, t_flags *flags, t_lst *tmp);
t_lst				**ft_settab(t_lst **tab, t_lst *lst);
t_lst				*ft_pathfinder(t_lst **tab, t_lst *lst, int *ok);
void				ft_display(t_lst *dup, t_lst *lst, int lem);

int					ft_isroom(t_lst *lst);
int					ft_howmuch(const char *str, unsigned char c);
int					ft_compare(t_lst *tube, t_lst *tab);
int					ft_notube(char **tube);
void				ft_runover(t_lst *lst, int *overstart, int *overend);
void				ft_replace(t_lst *lst, t_lst *tmp);
void				ft_free_lst(t_lst **lst);
void				ft_free_one(t_lst **lst);
void				ft_free_array(char **array);
void				ft_free_dup(t_lst **array);
t_lst				*ft_mvtend(t_lst *lst);

#endif
