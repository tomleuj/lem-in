/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 22:56:45 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/14 20:56:57 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *lil, size_t n)
{
	size_t i;
	size_t j;

	i = 0;
	j = 0;
	if (!*lil)
		return ((char *)big);
	while (i < n && big[i])
	{
		while (lil[j] == big[i + j] && (i + j) < n)
		{
			if (!lil[++j])
				return (char *)(big + i);
		}
		j = 0;
		i++;
	}
	return (NULL);
}
