/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 14:45:12 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/15 16:49:12 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *list;
	t_list *elem;

	if (!lst || !(*f))
		return (NULL);
	elem = (*f)(lst);
	list = elem;
	while (lst->next)
	{
		lst = lst->next;
		if (!(elem->next = (*f)(lst)))
			return (NULL);
		elem = elem->next;
	}
	return (list);
}
