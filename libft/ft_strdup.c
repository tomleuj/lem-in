/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 14:57:17 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/14 17:00:27 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	char *sdup;

	if (!(sdup = (char*)malloc(sizeof(*sdup) * (ft_strlen(s) + 1))))
		return (NULL);
	return (ft_strcpy(sdup, s));
}
