/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 12:31:13 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/07 12:35:28 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_file	*ft_set_list(const int fd, t_file **list)
{
	t_file *elem;
	t_file *end_list;

	if (fd < 0)
		return (NULL);
	elem = *list;
	end_list = NULL;
	while (elem)
	{
		if (fd == elem->fd)
			return (elem);
		end_list = elem;
		elem = elem->next;
	}
	if (!(elem = (t_file *)malloc(sizeof(*elem))))
		return (NULL);
	elem->fd = fd;
	elem->str = NULL;
	elem->next = NULL;
	if (*list)
		end_list->next = elem;
	else
		*list = elem;
	return (elem);
}

static int		ft_check_str(char **str, char **line)
{
	char *endl;

	if ((endl = ft_strchr(*str, '\n')))
	{
		*endl = '\0';
		if (!(*line = (ft_strdup(*str))))
			return (-1);
		if (!(endl = (ft_strdup(endl + 1))))
			return (-1);
		free(*str);
		*str = endl;
		endl = NULL;
		return (1);
	}
	return (0);
}

static int		ft_check_buff(char **str, char **line, char **buffer)
{
	char *endl;
	char *clean;

	if ((endl = ft_strchr(*buffer, '\n')))
	{
		*endl = '\0';
		if (!(*line = ft_strjoin(*str, *buffer)))
			return (-1);
		free(*str);
		if (!(*str = ft_strdup(endl + 1)))
			return (-1);
		free(*buffer);
		endl = NULL;
		return (1);
	}
	clean = *str;
	if (!(*str = ft_strjoin(*str, *buffer)))
		return (-1);
	free(clean);
	clean = NULL;
	return (0);
}

static int		ft_check_eof(t_file *elem, char **line,
		char **buffer, int char_nb)
{
	if (elem->str && *(elem->str) == '\0')
	{
		free(elem->str);
		elem->str = NULL;
	}
	free(*buffer);
	*buffer = NULL;
	if (char_nb == -1)
		return (-1);
	if (elem->str == NULL)
	{
		free(elem);
		return (0);
	}
	if (!(*line = ft_strdup(elem->str)))
		return (-1);
	free(elem->str);
	elem->str = NULL;
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	static t_file	*list = NULL;
	t_file			*elem;
	char			*buffer;
	int				char_nb;
	int				ret;

	if (!(elem = ft_set_list(fd, &list)))
		return (-1);
	if (elem->str)
	{
		if ((ret = ft_check_str(&elem->str, line)) == 1 || ret == -1)
			return (ret);
	}
	else
		elem->str = ft_strnew(0);
	if (!(elem->str) || !(buffer = ft_strnew(BUFF_SIZE)))
		return (-1);
	while ((char_nb = read(fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[char_nb] = '\0';
		if ((ret = ft_check_buff(&elem->str, line, &buffer)) == 1 || ret == -1)
			return (ret);
	}
	return (ft_check_eof(elem, line, &buffer, char_nb));
}
