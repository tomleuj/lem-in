/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 07:23:50 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/27 15:18:40 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_size(int *n, int *ng)
{
	size_t	size;
	int		tmp;

	size = 0;
	if (*n < 0)
	{
		*n *= -1;
		*ng = 1;
		size++;
	}
	tmp = *n;
	while (tmp /= 10)
		size++;
	return (++size);
}

char		*ft_itoa(int n)
{
	size_t	size;
	char	*set;
	char	*s;
	int		ng;

	ng = 0;
	set = "0123456789";
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	size = ft_size(&n, &ng);
	if (!(s = ft_strnew(size)))
		return (NULL);
	while (size--)
	{
		s[size] = set[n % 10];
		n /= 10;
	}
	if (ng)
		s[0] = '-';
	return (s);
}
