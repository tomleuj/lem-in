/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 19:44:12 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/11 19:53:06 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dst, const char *src, size_t n)
{
	char *ret;

	ret = dst;
	while (*dst)
		dst++;
	while (n-- && *src)
		*dst++ = *src++;
	*dst = '\0';
	return (ret);
}
