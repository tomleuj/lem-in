/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 20:39:33 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/26 21:03:55 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_words(char const *s, char c)
{
	int		flag;
	int		count;

	flag = 1;
	count = 0;
	while (*s)
	{
		if (flag && *s != c)
		{
			count++;
			flag = 0;
		}
		else if (*s == c)
			flag = 1;
		s++;
	}
	return (count);
}

static int		ft_len(char const *s, char c)
{
	int		len;

	len = 0;
	while (*s != c && *s)
	{
		len++;
		s++;
	}
	return (len);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**array;
	int		words;
	int		i;

	if (!s)
		return (NULL);
	i = 0;
	words = ft_words(s, c);
	if (!(array = (char **)malloc(sizeof(*array) * (words + 1))))
		return (NULL);
	while (i < words)
	{
		while (*s == c)
			s++;
		if (!(array[i] = ft_strsub(s, 0, ft_len(s, c))))
			return (NULL);
		s += ft_len(s, c);
		i++;
	}
	array[i] = NULL;
	return (array);
}
