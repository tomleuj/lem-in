/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handlers.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 18:19:43 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/09 11:19:50 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_lst	*ft_hndllem(t_flags *flags, t_lst *tmp)
{
	tmp->type = COMM;
	if (flags->l)
		flags->lem = ft_atoi(tmp->info);
	else
		tmp->type = FAIL;
	return (tmp);
}

static t_lst	*ft_hndlcomm(t_flags *flags, t_lst *tmp)
{
	if (tmp->type == START)
	{
		tmp->type = COMM;
		flags->s = 1;
	}
	else if (tmp->type == END)
	{
		tmp->type = COMM;
		flags->e = 1;
	}
	return (tmp);
}

static t_lst	*ft_hndlroom(t_flags *flags, t_lst *tmp)
{
	flags->l = 0;
	if (flags->s)
	{
		tmp->type = START;
		flags->s = 0;
	}
	if (flags->e)
	{
		tmp->type = END;
		flags->e = 0;
	}
	if (!flags->r)
		tmp->type = FAIL;
	return (tmp);
}

static t_lst	*ft_hndltube(t_flags *flags, t_lst *tmp)
{
	int		onereal;
	int		tworeal;
	char	**tube_info;
	t_lst	*lst;

	flags->l = 0;
	flags->r = 0;
	onereal = 0;
	tworeal = 0;
	lst = flags->lst;
	tube_info = ft_strsplit(tmp->info, '-');
	while (lst)
	{
		if (ft_strcmp(tube_info[0], lst->info) == 0)
			onereal = 1;
		if (ft_strcmp(tube_info[1], lst->info) == 0)
			tworeal = 1;
		lst = lst->next;
	}
	onereal = (ft_notube(tube_info)) ? 0 : onereal;
	ft_free_array(tube_info);
	if (!(onereal && tworeal))
		tmp->type = FAIL;
	return (tmp);
}

t_lst			*ft_hndlhex(t_flags *flags, t_lst *tmp)
{
	t_lst	*(*tab[6])(t_flags *, t_lst *);

	tab[0] = &ft_hndllem;
	tab[1] = &ft_hndlcomm;
	tab[2] = &ft_hndlcomm;
	tab[3] = &ft_hndlroom;
	tab[4] = &ft_hndltube;
	tab[5] = NULL;
	if (tmp->type <= 4)
		tmp = (*tab[tmp->type])(flags, tmp);
	return (tmp);
}
