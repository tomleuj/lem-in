/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dispatch.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 17:19:54 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/07 11:53:50 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_lst		*ft_remove(t_lst **lst, t_flags *flags, t_lst *tmp)
{
	t_lst	*tmp_bis;

	tmp_bis = *lst;
	if (tmp_bis != tmp)
	{
		while (tmp_bis->next != tmp)
			tmp_bis = tmp_bis->next;
		tmp_bis->next = tmp->next;
		ft_free_one(&tmp);
		tmp = tmp_bis->next;
	}
	else
	{
		*lst = tmp->next;
		ft_free_one(&tmp);
		tmp = *lst;
	}
	flags->lst = *lst;
	return (tmp);
}

static t_lst		*ft_kill(t_lst **lst, t_flags *flags, t_lst *tmp)
{
	t_lst	*tmp_bis;

	tmp_bis = NULL;
	if (*lst == tmp)
		ft_free_lst(lst);
	else
	{
		tmp_bis = *lst;
		while (tmp_bis->next != tmp)
			tmp_bis = (tmp_bis)->next;
		tmp_bis->next = NULL;
		ft_free_lst(&tmp);
	}
	flags->lst = *lst;
	return (NULL);
}

t_lst				*ft_dispatch(t_lst **lst, t_flags *flags, t_lst *tmp)
{
	if (tmp->type == COMM)
		tmp = ft_remove(lst, flags, tmp);
	else if (tmp->type == FAIL)
		tmp = ft_kill(lst, flags, tmp);
	else
		tmp = tmp->next;
	return (tmp);
}
