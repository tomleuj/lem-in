/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 18:52:11 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/08 14:32:00 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_lst		*ft_set_lst(char *line, t_lst *tmp)
{
	t_lst *elem;

	if (!(elem = (t_lst *)malloc(sizeof(*elem))))
		return (NULL);
	elem->type = FAIL;
	elem->dst = -1;
	elem->info = ft_strdup(line);
	elem->side = NULL;
	elem->path = NULL;
	elem->pipe = NULL;
	elem->road = NULL;
	elem->next = NULL;
	elem->rewind = NULL;
	if (tmp)
		tmp->next = elem;
	return (elem);
}

static t_lst		*ft_read_stdin(t_lst *lst)
{
	t_lst	*tmp;
	char	*line;

	tmp = NULL;
	while (get_next_line(0, &line) > 0)
	{
		if (!(tmp = ft_set_lst(line, tmp)))
		{
			free(line);
			ft_free_lst(&lst);
			return (lst);
		}
		free(line);
		if (!lst)
			lst = tmp;
	}
	return (lst);
}

static t_lst		*ft_lstdup(t_lst *lst)
{
	t_lst *ret;
	t_lst *top;

	ret = NULL;
	top = NULL;
	while (lst)
	{
		ret = ft_set_lst(lst->info, ret);
		ret->type = lst->type;
		if (!top)
			top = ret;
		lst = lst->next;
	}
	return (top);
}

static t_lst		*ft_isset(t_lst *lst, int lem, int *ok)
{
	int		s_set;
	int		e_set;
	t_lst	*tmp;

	s_set = 0;
	e_set = 0;
	tmp = lst;
	while (tmp)
	{
		if (tmp->type == START)
			s_set = 1;
		if (tmp->type == END)
			e_set = 1;
		tmp = tmp->next;
	}
	if (!(s_set && e_set && lem))
		*ok = 0;
	return (lst);
}

t_lst				*ft_build_hex(t_lst *lst, t_lst **ret, t_lst ***tab)
{
	int		ok;
	int		lem;

	ok = 1;
	lem = 0;
	lst = ft_read_stdin(lst);
	lst = ft_sort_lst(lst);
	*ret = ft_lstdup(lst);
	lst = ft_filter(lst, &lem);
	lst = ft_isset(lst, lem, &ok);
	*tab = ft_settab(*tab, lst);
	if (ok)
		lst = ft_pathfinder(*tab, lst, &ok);
	ok ? ft_display(*ret, lst, lem) : ft_putendl("Error");
	return (lst);
}
