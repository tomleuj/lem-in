/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:14:06 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/06 17:24:04 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_lst			*ft_foreward(t_lst *curse, t_lst *pre_room, int dst)
{
	t_lst	*tmp;

	tmp = NULL;
	while (curse->path)
	{
		curse = curse->path;
		if (curse->pipe->dst == -1)
		{
			tmp = curse;
			curse = curse->pipe;
			curse->dst = dst + 1;
			curse->road = pre_room;
			if (curse->type == END)
				return (curse);
			curse = tmp;
		}
	}
	return (curse);
}

static int				ft_tabsize(t_lst **tab)
{
	int size;

	size = 0;
	while (tab[size])
		size++;
	return (size);
}

static void				ft_rchend(t_lst **tab, t_lst *curse)
{
	int		i;
	int		dst;
	int		size;
	t_lst	*pre_room;

	dst = 0;
	pre_room = curse;
	size = ft_tabsize(tab);
	while (dst < size && curse && curse->type != END)
	{
		i = 0;
		while (tab[i] && curse->type != END)
		{
			if (tab[i]->dst == dst)
			{
				pre_room = tab[i];
				curse = tab[i];
				curse = ft_foreward(curse, pre_room, dst);
			}
			i++;
		}
		dst++;
	}
}

static void				ft_rewind(t_lst *lst)
{
	t_lst	*tmp;

	tmp = lst;
	while (tmp->type != END)
		tmp = tmp->next;
	while (tmp->type != START && tmp->road)
	{
		if (tmp->type == END)
			tmp->rewind = NULL;
		tmp->road->rewind = tmp;
		tmp = tmp->road;
	}
}

t_lst					*ft_pathfinder(t_lst **tab, t_lst *lst, int *ok)
{
	int	i;

	i = 0;
	while (tab[i] && tab[i]->type != START)
		i++;
	if (!tab[i])
		return (NULL);
	tab[i]->dst = 0;
	ft_rchend(tab, tab[i]);
	i = 0;
	while (tab[i] && tab[i]->type != END)
		i++;
	if (!tab[i] || tab[i]->dst == -1)
		*ok = 0;
	else
		ft_rewind(lst);
	return (lst);
}
