/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 16:48:56 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/06 16:37:46 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_lst			**ft_gettab(t_lst *lst)
{
	size_t	i;
	size_t	size;
	t_lst	*tmp;
	t_lst	**tab;

	i = 0;
	size = 0;
	tmp = lst;
	tab = NULL;
	while (tmp && tmp->type != TUBE)
	{
		size++;
		tmp = tmp->next;
	}
	if (!(tab = (t_lst **)malloc(sizeof(*tab) * (size + 1))))
		return (NULL);
	tmp = lst;
	while (i < size && tmp && tmp->type != TUBE)
	{
		tab[i++] = tmp;
		tmp = tmp->next;
	}
	tab[size] = NULL;
	return (tab);
}

static t_lst			*ft_lstdup(t_lst *lst)
{
	t_lst	*dup;

	dup = NULL;
	if (!(dup = (t_lst *)malloc(sizeof(*dup))))
		return (NULL);
	dup->type = DUP;
	dup->dst = -1;
	if (!(dup->info = ft_strdup(lst->info)))
		return (NULL);
	dup->side = NULL;
	dup->path = NULL;
	dup->pipe = NULL;
	dup->road = NULL;
	dup->next = NULL;
	dup->rewind = NULL;
	return (dup);
}

static t_lst			*ft_formatpipe(t_lst **tab, t_lst *curse, t_lst *tube)
{
	int		i;
	int		j;
	char	**tube_pipe;

	i = 0;
	j = 0;
	tube_pipe = NULL;
	tube_pipe = ft_strsplit(tube->info, '-');
	while (tube_pipe[i])
	{
		if (ft_strcmp(tube_pipe[i], curse->info) != 0)
		{
			while (tab[j])
			{
				if (ft_strcmp(tab[j]->info, tube_pipe[i]) == 0)
					tube->pipe = tab[j];
				j++;
			}
		}
		i++;
	}
	ft_free_array(tube_pipe);
	return (tube);
}

static void				ft_filltab(t_lst **tab, t_lst *lst)
{
	int		i;
	t_lst	*end;
	t_lst	*dup;

	i = 0;
	end = NULL;
	dup = NULL;
	while (tab && tab[i])
	{
		if (!dup && ft_compare(lst, tab[i]))
		{
			end = ft_mvtend(tab[i]);
			end->path = lst;
			lst = ft_formatpipe(tab, tab[i], lst);
			dup = ft_lstdup(lst);
		}
		else if (dup && ft_compare(lst, tab[i]))
		{
			end = ft_mvtend(tab[i]);
			dup = ft_formatpipe(tab, tab[i], dup);
			end->path = dup;
		}
		i++;
	}
}

t_lst					**ft_settab(t_lst **tab, t_lst *lst)
{
	tab = ft_gettab(lst);
	while (lst)
	{
		if (lst->type == TUBE)
			ft_filltab(tab, lst);
		lst = lst->next;
	}
	return (tab);
}
