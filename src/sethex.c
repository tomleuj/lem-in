/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sethex.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 16:40:47 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/08 14:54:51 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void			ft_set_flags(t_flags *flags, t_lst *lst)
{
	flags->l = 1;
	flags->r = 1;
	flags->s = 0;
	flags->e = 0;
	flags->lst = lst;
}

static t_lst		*ft_isdouble(t_lst *lst, t_lst *tmp, char *str)
{
	int		isdouble;
	t_lst	*start;

	isdouble = 0;
	start = lst;
	while (lst != tmp)
	{
		if (ft_strcmp(str, lst->info) == 0)
			isdouble = 1;
		lst = lst->next;
	}
	if (isdouble == 1 && (tmp->type != START && tmp->type != END))
		tmp->type = COMM;
	else if (isdouble == 1 && (tmp->type == START || tmp->type == END))
		ft_replace(start, tmp);
	return (tmp);
}

static t_lst		*ft_isrev(t_lst *lst, t_lst *tmp)
{
	int		pipe_rev;
	char	*reverse;
	char	*pipe;
	char	*endl;

	endl = NULL;
	if (!(reverse = ft_strnew(ft_strlen(tmp->info))))
		return (NULL);
	pipe = ft_strchr(tmp->info, '-');
	endl = ft_strchr(tmp->info, '\0');
	pipe_rev = endl - pipe - 1;
	ft_memmove(reverse, pipe + 1, pipe_rev);
	ft_memmove(reverse + (endl - pipe), tmp->info, pipe - tmp->info);
	*(reverse + pipe_rev) = '-';
	tmp = ft_isdouble(lst, tmp, tmp->info);
	tmp = ft_isdouble(lst, tmp, reverse);
	free(reverse);
	return (tmp);
}

static t_lst		*ft_hndldopple(t_lst *lst, t_lst *tmp)
{
	if (tmp->type == TUBE)
		tmp = ft_isrev(lst, tmp);
	if (tmp->type == ROOM || tmp->type == START || tmp->type == END)
		tmp = ft_isdouble(lst, tmp, tmp->info);
	return (tmp);
}

t_lst				*ft_filter(t_lst *lst, int *lem)
{
	int		overstart;
	int		overend;
	t_flags	flags;
	t_lst	*tmp;

	overstart = 0;
	overend = 0;
	flags.lem = 0;
	ft_set_flags(&flags, lst);
	tmp = lst;
	while (tmp)
	{
		tmp = ft_hndlhex(&flags, tmp);
		tmp = ft_hndldopple(lst, tmp);
		tmp = ft_dispatch(&lst, &flags, tmp);
	}
	ft_set_flags(&flags, lst);
	tmp = lst;
	while (tmp)
		tmp = ft_dispatch(&lst, &flags, tmp);
	if (lst)
		ft_runover(lst, &overstart, &overend);
	*lem = flags.lem;
	return (lst);
}
