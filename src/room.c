/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 16:37:29 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/27 16:13:02 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int			ft_cleaner(char **room_info)
{
	int i;

	i = 0;
	while (room_info[i])
		free(room_info[i++]);
	free(room_info);
	return (0);
}

static int			ft_isxy(char **room_info)
{
	int		i;
	int		n;
	char	*tmp;

	i = 1;
	n = 0;
	tmp = NULL;
	while (i <= 2)
	{
		n = ft_atoi(room_info[i]);
		tmp = ft_itoa(n);
		if (ft_strcmp(tmp, room_info[i]) != 0)
		{
			free(tmp);
			return (0);
		}
		free(tmp);
		i++;
	}
	return (1);
}

int					ft_isroom(t_lst *lst)
{
	int		i;
	char	**room_info;

	i = 0;
	room_info = NULL;
	if (ft_howmuch(lst->info, ' ') != 2)
		return (0);
	room_info = ft_strsplit(lst->info, ' ');
	while (room_info[i])
		i++;
	if (i != 3)
		return (ft_cleaner(room_info));
	if (room_info[0][0] == 'L')
		return (ft_cleaner(room_info));
	if (ft_strchr(room_info[0], '-'))
		return (ft_cleaner(room_info));
	if (!ft_isxy(room_info))
		return (ft_cleaner(room_info));
	ft_free_array(room_info);
	return (1);
}
