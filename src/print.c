/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/04 14:14:29 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/08 15:45:00 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void			ft_printlem(t_lst *tmp, int ant)
{
	if (ant > 1 && tmp->rewind)
		ft_printlem(tmp->rewind, ant - 1);
	ft_putchar('L');
	ft_putnbr(ant);
	ft_putchar('-');
	ft_putstr(tmp->info);
	ft_putchar(' ');
}

static void			ft_antman(t_lst *tmp, int lem)
{
	int		ant;

	ant = 1;
	if (tmp)
	{
		while (tmp->type != START)
			tmp = tmp->next;
		tmp = tmp->rewind;
	}
	while (tmp)
	{
		ft_printlem(tmp, ant);
		ft_putchar('\n');
		if (ant == lem)
			tmp = tmp->rewind;
		else
			ant++;
	}
}

static t_lst		*ft_norme(t_lst *ret, t_lst **lst)
{
	if (ret->type == FAIL)
		return (ret);
	else if (ft_strcmp(ret->info, (*lst)->info) != 0)
		ret = ret->next;
	else
	{
		if (*lst && (*lst)->type == START)
			ft_putendl("##start");
		else if (*lst && (*lst)->type == END)
			ft_putendl("##end");
		ft_putstr(ret->info);
		if (ret->type == ROOM)
			ft_putchar(' ');
		ft_putendl((*lst)->side);
		ret = ret->next;
		*lst = (*lst)->next;
	}
	return (ret);
}

static void			ft_printhex(t_lst *ret, t_lst *lst)
{
	t_lst *tmp;

	tmp = ret;
	while (ret && lst)
	{
		while (ret->next && (ret->type == COMM || ret->type == LEM))
		{
			ft_putendl(ret->info);
			ret = ret->next;
		}
		ret = ft_norme(ret, &lst);
		if (ret && ret->type == FAIL)
			ret = NULL;
	}
	while (ret)
	{
		if (ret->type == COMM)
		{
			ft_putendl(ret->info);
			ret = ret->next;
		}
		else
			ret = NULL;
	}
}

void				ft_display(t_lst *ret, t_lst *lst, int lem)
{
	ft_printhex(ret, lst);
	ft_putchar('\n');
	ft_antman(lst, lem);
}
