/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   siders.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 14:51:50 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/08 16:00:37 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_lst		*ft_mvtend(t_lst *lst)
{
	t_lst *tmp;

	tmp = lst;
	while (tmp->path)
		tmp = tmp->path;
	return (tmp);
}

int			ft_howmuch(const char *str, unsigned char c)
{
	int	i;
	int	nb;

	i = 0;
	nb = 0;
	if (!str)
		return (0);
	while (str[i])
	{
		if (str[i] == c)
			nb++;
		i++;
	}
	return (nb);
}

int			ft_compare(t_lst *tube, t_lst *tab)
{
	char	**cut;

	cut = ft_strsplit(tube->info, '-');
	if (ft_strcmp(cut[0], tab->info) == 0)
	{
		ft_free_array(cut);
		return (1);
	}
	if (ft_strcmp(cut[1], tab->info) == 0)
	{
		ft_free_array(cut);
		return (1);
	}
	ft_free_array(cut);
	return (0);
}

void		ft_runover(t_lst *lst, int *overstart, int *overend)
{
	if (lst->next)
		ft_runover(lst->next, overstart, overend);
	if (*overstart == 1 && lst->type == START)
		lst->type = ROOM;
	else if (*overend == 1 && lst->type == END)
		lst->type = ROOM;
	if (lst->type == START)
		*overstart = 1;
	else if (lst->type == END)
		*overend = 1;
}

void		ft_replace(t_lst *lst, t_lst *tmp)
{
	while (lst != tmp)
	{
		if (ft_strcmp(lst->info, tmp->info) == 0)
			lst->type = COMM;
		lst = lst->next;
	}
}
