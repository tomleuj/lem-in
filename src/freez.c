/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   freez.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 14:51:06 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/09 11:18:07 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		ft_free_lst(t_lst **lst)
{
	t_lst *tmp;

	while (*lst)
	{
		tmp = (*lst)->next;
		free((*lst)->info);
		free((*lst)->side);
		free(*lst);
		*lst = NULL;
		*lst = tmp;
	}
}

void		ft_free_one(t_lst **lst)
{
	free((*lst)->info);
	free((*lst)->side);
	free(*lst);
	*lst = NULL;
}

void		ft_free_array(char **array)
{
	int i;

	i = 0;
	while (array[i])
		free(array[i++]);
	free(array);
}

void		ft_free_dup(t_lst **array)
{
	int		i;
	t_lst	*lst;
	t_lst	*tmp;

	i = 0;
	lst = NULL;
	tmp = NULL;
	while (array[i])
	{
		lst = array[i];
		while (lst)
		{
			tmp = lst->path;
			if (lst->type == DUP)
				ft_free_one(&lst);
			lst = tmp;
		}
		i++;
	}
}

int			ft_notube(char **tube)
{
	if (ft_strcmp(tube[0], tube[1]) == 0)
		return (1);
	return (0);
}
