/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 17:03:26 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/06 16:26:44 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			main(void)
{
	t_lst *lst;
	t_lst *ret;
	t_lst **tab;

	lst = NULL;
	ret = NULL;
	tab = NULL;
	lst = ft_build_hex(lst, &ret, &tab);
	ft_free_dup(tab);
	free(tab);
	ft_free_lst(&lst);
	ft_free_lst(&ret);
	return (0);
}
