/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 16:37:04 by tlejeune          #+#    #+#             */
/*   Updated: 2017/03/05 17:43:55 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_lst		*ft_set_comm(t_lst *lst)
{
	if (ft_strcmp(lst->info, "##start") == 0)
		lst->type = START;
	else if (ft_strcmp(lst->info, "##end") == 0)
		lst->type = END;
	else
		lst->type = COMM;
	return (lst);
}

static t_lst		*ft_set_room(t_lst *lst)
{
	char	*tmp;

	tmp = NULL;
	lst->type = ROOM;
	tmp = ft_strchr(lst->info, ' ');
	lst->side = ft_strdup(tmp + 1);
	while (*tmp)
	{
		*tmp = '\0';
		tmp++;
	}
	return (lst);
}

static int			ft_istube(t_lst *lst)
{
	int		i;
	char	**tube_info;

	i = 0;
	tube_info = NULL;
	if (ft_strchr(lst->info, ' '))
		return (0);
	if (ft_howmuch(lst->info, '-') != 1)
		return (0);
	tube_info = ft_strsplit(lst->info, '-');
	while (tube_info[i])
		i++;
	ft_free_array(tube_info);
	if (i != 2)
		return (0);
	return (1);
}

static int			ft_islem(t_lst *lst)
{
	int		n;
	char	*tmp;

	n = 0;
	tmp = NULL;
	n = ft_atoi(lst->info);
	tmp = ft_itoa(n);
	if (ft_strcmp(tmp, lst->info) != 0 || n <= 0)
	{
		free(tmp);
		return (0);
	}
	free(tmp);
	return (1);
}

t_lst				*ft_sort_lst(t_lst *lst)
{
	t_lst *tmp;

	tmp = lst;
	while (tmp)
	{
		if (tmp->info[0] == '#')
			tmp = ft_set_comm(tmp);
		else if (ft_isroom(tmp))
			tmp = ft_set_room(tmp);
		else if (ft_istube(tmp))
			tmp->type = TUBE;
		else if ((ft_islem(tmp)))
			tmp->type = LEM;
		else
			tmp->type = FAIL;
		tmp = tmp->next;
	}
	return (lst);
}
