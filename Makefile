# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/25 11:31:07 by tlejeune          #+#    #+#              #
#    Updated: 2017/03/04 18:32:09 by tlejeune         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in
CC = gcc
FLAGS = -Wall -Wextra -Werror
DEBUG = yes

ifeq ($(DEBUG),yes)
	FLAGS += -g
endif

TRASH = info .DS_Store lem-in.dSYM

SRC_PATH = ./src/
INCLUDES_PATH = ./includes/
OBJ_PATH = ./obj/

LIBFT_PATH = ./libft/
LIBFT_H = -I$(LIBFT_PATH)
LIBFT_A = $(LIBFT_PATH)libft.a
LIBFT_LINK = -L$(LIBFT_PATH) -lft


SRC = main.c\
	  parse.c\
	  sort.c\
	  room.c\
	  sethex.c\
	  handlers.c\
	  dispatch.c\
	  tab.c\
	  algo.c\
	  print.c\
	  freez.c\
	  siders.c\

OBJ	= $(addprefix $(OBJ_PATH),$(SRC:.c=.o))

all: $(NAME)

$(NAME): obj $(LIBFT_A) $(OBJ)
	$(CC) $(FLAGS) -o $@ $(LIBFT_A) $(OBJ)

obj:
	mkdir $(OBJ_PATH)

$(LIBFT_A):
	make -C $(LIBFT_PATH)

$(OBJ_PATH)%.o:$(SRC_PATH)%.c
	$(CC) $(FLAGS) $(LIBFT_H) -I$(INCLUDES_PATH) -o $@ -c $<

clean:
	rm -rf $(OBJ_PATH)
	rm -rf $(TRASH)
	make clean -C $(LIBFT_PATH)

fclean: clean
	rm -rf $(NAME)
	make fclean -C $(LIBFT_PATH)

re: fclean all
